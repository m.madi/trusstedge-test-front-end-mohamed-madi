import HeroSection from "./Components/HomePage/HeroSection";
import DiscountsSection from "./Components/HomePage/DiscountsSection";
import HowItWorks from "./Components/HomePage/Works";
import OurBlog from "./Components/HomePage/OurBlog";
import EnjoyExperience from "./Components/HomePage/EnjoyExperience";


export default function HomePage() {
    return (
        <>
            <HeroSection />
            <DiscountsSection/>
            <HowItWorks/>
            <OurBlog/>
            <EnjoyExperience/>
        </>
    )
}