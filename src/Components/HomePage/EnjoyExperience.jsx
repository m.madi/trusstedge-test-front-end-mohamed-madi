
import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import AspectRatio from '@mui/joy/AspectRatio';

// Images
import AppStoreWhiteImage from '../../assets/app-store-white.png';
import GooglePlayWhiteImage from '../../assets/google-play-white.png';
import LastLP from '../../assets/banner-LP.png';

const TypographyWhite = styled(Typography)(({ theme }) => ({
    color: '#FFF'
}));

const TypographyColor = styled(Typography)(({ theme }) => ({
    color: '#E0E0E0'
}));

const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));

const GridStyle = styled(Grid)(({ theme }) => ({
    background: '-webkit-linear-gradient(left, #861889, #F8B03A, #F8B03A)',
    borderRadius: '20px',
}));

export default function EnjoyExperience() {
    return (
        <Grid
            container
            spacing={2}
            // justifyContent="space-between"
            alignItems="center"
            justifyContent="center"
            columns={12}
            sx={{ flexGrow: 1, marginTop: '100px' }}>
            <GridStyle xs={8}
                container
            >
                <Grid xs={12} sm={12} md={6} sx={{ padding: '20px 3%' }}>
                    <CardStyle variant="plain">
                        <TypographyColor level="h2" sx={{ textAlign: 'start' }}>
                            Enjoy the experience of shopping more and <TypographyWhite variant="plain">paying less!</TypographyWhite>
                        </TypographyColor>
                    </CardStyle>

                    <Stack direction="row" spacing={1}>
                        <Button variant="text">
                            <img
                                src={AppStoreWhiteImage}
                                srcSet={AppStoreWhiteImage}
                                alt="App Store"
                            />
                        </Button>
                        <Button variant="text">
                            <img
                                src={GooglePlayWhiteImage}
                                srcSet={GooglePlayWhiteImage}
                                alt="Google Play"
                            />
                        </Button>
                    </Stack>
                </Grid>

                <Grid xs={12} sm={12} md={6}>
                    <AspectRatio
                        objectFit="contain"
                        variant="plain"
                        sx={{
                            width: '100%',
                            marginTop: '20px'
                        }}>
                        <img
                            src={LastLP}
                            srcSet={LastLP}
                            alt="Logo"
                        />
                    </AspectRatio>
                </Grid>
            </GridStyle>
        </Grid>
    )
}