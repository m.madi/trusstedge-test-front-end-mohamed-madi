// Discover the best discounts and offers nears you

import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';


// Images
import ContentImage from '../../assets/content.png';


const Textgradient = styled(Typography)(({ theme }) => ({
    background: '-webkit-linear-gradient(right, #861889, #F8B03A, #F8B03A)',
    'border-radius': '20px',
    padding: '7px 5%',
    color: '#FFF'
}));

const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));

const GridStyle = styled(Grid)(({ theme }) => ({
    borderTop:'3px solid #EEE',
    cursor:'pointer',
    transition:'all .5 ease-in-out',
    marginTop:'50px',
    ':hover':{
        'border-image': 'linear-gradient(to right, #861889, #F8B03A) 30',
        borderTop:'3px',
        borderLeft:'0',
        borderRight:'0',
        borderBottom:'0',
        'borderStyle': 'solid',
    }
}));


export default function HowItWorks() {
    return (
        <Grid
            container
            spacing={2}
            // justifyContent="space-between"
            alignItems="center"
            justifyContent="center"
            columns={12}
            sx={{ flexGrow: 1, marginTop: '100px' }}>
            <Grid xs={12} sm={8} md={6}>
                <CardStyle variant="plain">
                    <Textgradient level="h5" width={'30%'} margin={'auto'}>How it works</Textgradient>
                    <Typography level="h1" sx={{ margin: '20px 0' }}>
                        Amazing online offers are waiting for you!
                    </Typography>
                    <Typography level="h3" sx={{ margin: '20px 0' }}>
                        In 3 simple steps, you can saving
                    </Typography>
                    <Typography>
                        Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality, though, the unity and coherence of ideas among sentences is what constitutes a paragraph.
                    </Typography>
                </CardStyle>
                <Grid xs={2}>
                    <img
                        width={'100%'}
                        src={ContentImage}
                        srcSet={ContentImage}
                        alt="Logo"
                    />
                </Grid>
            </Grid>

            <Grid
                container
                // spacing={{ xs: 2, md: 3 }}
                // columns={{ xs: 4, sm: 8, md: 12 }}
                // sx={{ flexGrow: 1 }}
                xs={12}
            >
                
                <GridStyle xs={12} sm={12} md={4} sx={{ borderTop: '3px solid    #EEE' }}>
                    <CardStyle variant="plain">
                        {/* <Textgradient level="h5" width={'30%'} margin={'auto'}>How it works</Textgradient> */}
                        <Typography level="h3" sx={{ margin: '20px 0' }}>
                            Log in
                        </Typography>
                        <Typography>
                            Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality
                        </Typography>
                    </CardStyle>
                </GridStyle>
                <GridStyle xs={12} sm={12} md={4} sx={{ borderTop: '3px solid #EEE' }}>
                    <CardStyle variant="plain">
                        {/* <Textgradient level="h5" width={'30%'} margin={'auto'}>How it works</Textgradient> */}
                        <Typography level="h3" sx={{ margin: '20px 0' }}>
                            Search for deals
                        </Typography>
                        <Typography>
                            Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality
                        </Typography>
                    </CardStyle>
                </GridStyle>
                <GridStyle xs={12} sm={12} md={4} sx={{ borderTop: '3px solid #EEE' }}>
                    <CardStyle variant="plain">
                        {/* <Textgradient level="h5" width={'30%'} margin={'auto'}>How it works</Textgradient> */}
                        <Typography level="h3" sx={{ margin: '20px 0' }}>
                            Save more
                        </Typography>
                        <Typography>
                            Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality
                        </Typography>
                    </CardStyle>
                </GridStyle>
            </Grid>

        </Grid>
    );
}