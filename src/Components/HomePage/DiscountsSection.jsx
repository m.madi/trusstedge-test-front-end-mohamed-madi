// Discover the best discounts and offers nears you

import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';
import Avatar from '@mui/joy/Avatar';



// Images
import Mockup from '../../assets/mockup-1.jpeg';
import FeaturedIcon from '../../assets/Featured-icon-1.png';
import FeaturedIcon2 from '../../assets/Featured-icon-2.png';
import FeaturedIcon3 from '../../assets/Featured-icon-3.png';
import FeaturedIcon4 from '../../assets/Featured-icon-4.png';
import FeaturedIcon5 from '../../assets/Featured-icon-5.png';
import FeaturedIcon6 from '../../assets/Featured-icon-6.png';


const Textgradient = styled(Typography)(({ theme }) => ({
    background: '-webkit-linear-gradient(right, #861889, #F8B03A, #F8B03A)',
    'border-radius': '20px',
    padding: '7px 5%',
    color: '#FFF'
}));

const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));

export default function DiscountsSection() {
    return (
        <Grid
            container
            spacing={2}
            justifyContent="space-between"
            alignItems="center"
            columns={12}
            sx={{ flexGrow: 1, marginTop: '100px' }}>
            <Grid xs={12} sm={8} md={6}>
                <CardStyle variant="plain">
                    <Textgradient level="h5" textAlign={'start'}>Discover the best discounts and offers nears you</Textgradient>
                    <Typography level="h1" textAlign={'start'} sx={{ margin: '20px 0' }}>
                        Enjoy the art of purchasing and saving!
                    </Typography>
                    <Typography textAlign={'start'}>
                        Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality, though, the unity and coherence of ideas among sentences is what constitutes a paragraph.
                    </Typography>
                </CardStyle>

            </Grid>

            <Grid xs={12} sm={8}
                container
                rowSpacing={2}
                columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            >

                <Grid xs={6}>
                    <CardStyle variant="plain">
                        <Avatar alt="icon" src={FeaturedIcon} />
                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={6}>
                    <CardStyle variant="plain" sx={{ backgroundColor: 'none' }}>
                        <Avatar alt="icon" src={FeaturedIcon2} />
                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={6}>
                    <CardStyle variant="plain">
                        <Avatar alt="icon" src={FeaturedIcon3} />

                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={6}>
                    <CardStyle variant="plain">
                        <Avatar alt="icon" src={FeaturedIcon4} />

                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={6}>
                    <CardStyle variant="plain">
                        <Avatar alt="icon" src={FeaturedIcon5} />

                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={6}>
                    <CardStyle variant="plain">
                        <Avatar alt="icon" src={FeaturedIcon6} />

                        <Typography level="title-md" textAlign={'start'}>Restaurants & Cafes</Typography>
                        <Typography textAlign={'start'}>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                    </CardStyle>
                </Grid>
            </Grid>

            <Grid xs={4} sm={4}>
                <img
                    width={'100%'}
                    src={Mockup}
                    srcSet={Mockup}
                    alt="Logo"
                />
            </Grid>
        </Grid>
    );
}