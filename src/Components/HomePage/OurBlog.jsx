
import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';

import Slider from "react-slick";

// Slider
import AspectRatio from '@mui/joy/AspectRatio';
import CardContent from '@mui/joy/CardContent';
import CardOverflow from '@mui/joy/CardOverflow';

// Icons
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';


//// Images
import SliderImageOne from '../../assets/logo-image.png';
import SliderImageTwo from '../../assets/logo-image-2.png';
import SliderImageThree from '../../assets/logo-image-3.png';
import SliderImageFour from '../../assets/logo-image-4.png';

const Textgradient = styled(Typography)(({ theme }) => ({
    background: '-webkit-linear-gradient(right, #861889, #F8B03A, #F8B03A)',
    'border-radius': '20px',
    padding: '7px 5%',
    color: '#FFF'
}));

const TextColorSlider = styled(Typography)(({ theme }) => ({
    color: '#861889'
}));


const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));

export default function OurBlog() {
    const settings = {
        dots: true,
        arrows:true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <ArrowCircleRightIcon />,
        prevArrow: <ArrowCircleLeftIcon />,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Grid
            container
            spacing={2}
            // justifyContent="space-between"
            alignItems="flex-start"
            justifyContent="space-between"
            columns={12}
            sx={{ flexGrow: 1, marginTop: '100px' }}>
            <Grid xs={12} md={6}>
                <CardStyle variant="plain">
                    <Textgradient level="h5" width={'30%'}>Our Blog</Textgradient>
                    <Typography level="h1" sx={{ margin: '20px 0', textAlign: 'start' }}>
                        Amazing online offers are waiting for you!
                    </Typography>
                </CardStyle>
            </Grid>
            <Grid xs={12} md={4}>
                <CardStyle variant="plain">
                    <Typography sx={{ margin: '20px 0', textAlign: 'start' }}>
                        Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences
                    </Typography>
                </CardStyle>
            </Grid>
            



            <Grid xs={12}>
                <div>
                    <Slider {...settings}>
                        <div>
                            <CardStyle variant="plain" sx={{ width: 320 }}>
                                <CardOverflow>
                                    <AspectRatio ratio="2">
                                        <img
                                            src={SliderImageOne}
                                            srcSet={SliderImageOne}
                                            loading="lazy"
                                            alt=""
                                        />
                                    </AspectRatio>
                                </CardOverflow>
                                <CardContent sx={{textAlign:"start"}}>
                                    <TextColorSlider level="title-md">Olivia Rhya.20Jan 2022</TextColorSlider>
                                    <Typography level="h4">Blog Title goes here</Typography>
                                    <Typography>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                                </CardContent>
                            </CardStyle>
                        </div>

                        <div>
                            <CardStyle variant="plain" sx={{ width: 320 }}>
                                <CardOverflow>
                                    <AspectRatio ratio="2">
                                        <img
                                            src={SliderImageTwo}
                                            srcSet={SliderImageTwo}
                                            loading="lazy"
                                            alt=""
                                        />
                                    </AspectRatio>
                                </CardOverflow>
                                <CardContent sx={{textAlign:"start"}}>
                                    <TextColorSlider level="title-md">Olivia Rhya.20Jan 2022</TextColorSlider>
                                    <Typography level="h4">Blog Title goes here</Typography>
                                    <Typography>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                                </CardContent>

                            </CardStyle>
                        </div>
                        
                        <div>
                            <CardStyle variant="plain" sx={{ width: 320 }}>
                                <CardOverflow>
                                    <AspectRatio ratio="2">
                                        <img
                                            src={SliderImageThree}
                                            srcSet={SliderImageThree}
                                            loading="lazy"
                                            alt=""
                                        />
                                    </AspectRatio>
                                </CardOverflow>
                                <CardContent sx={{textAlign:"start"}}>
                                    <TextColorSlider level="title-md">Olivia Rhya.20Jan 2022</TextColorSlider>
                                    <Typography level="h4">Blog Title goes here</Typography>
                                    <Typography>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                                </CardContent>

                            </CardStyle>
                        </div>

                        <div>
                            <CardStyle variant="plain" sx={{ width: 320 }}>
                                <CardOverflow>
                                    <AspectRatio ratio="2">
                                        <img
                                            src={SliderImageFour}
                                            srcSet={SliderImageFour}
                                            loading="lazy"
                                            alt=""
                                        />
                                    </AspectRatio>
                                </CardOverflow>
                                <CardContent sx={{textAlign:"start"}}>
                                    <TextColorSlider level="title-md">Olivia Rhya.20Jan 2022</TextColorSlider>
                                    <Typography level="h4">Blog Title goes here</Typography>
                                    <Typography>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                                </CardContent>
                            </CardStyle>
                        </div>

                        <div>
                            <CardStyle variant="plain" sx={{ width: 320 }}>
                                <CardOverflow>
                                    <AspectRatio ratio="2">
                                        <img
                                            src={SliderImageFour}
                                            srcSet={SliderImageFour}
                                            loading="lazy"
                                            alt=""
                                        />
                                    </AspectRatio>
                                </CardOverflow>
                                <CardContent sx={{textAlign:"start"}}>
                                    <TextColorSlider level="title-md">Olivia Rhya.20Jan 2022</TextColorSlider>
                                    <Typography level="h4">Blog Title goes here</Typography>
                                    <Typography>Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences</Typography>
                                </CardContent>
                            </CardStyle>
                        </div>
                    </Slider>
                </div>
            </Grid>

        </Grid>
    )
}