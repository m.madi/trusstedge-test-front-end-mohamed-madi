import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';
import AspectRatio from '@mui/joy/AspectRatio';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';



// Images
import LastLP from '../../assets/last-LP.png';
import AppStoreImage from '../../assets/appStore.png';
import GooglePlayImage from '../../assets/googlePlay.png';

const Textgradient = styled(Typography)(({ theme }) => ({
  background: '-webkit-linear-gradient(left, #861889, #F8B03A, #F8B03A)',
  '-webkit-background-clip': 'text',
  '-webkit-text-fill-color': 'transparent'
}));

export default function HeroSection() {
  return (
    <Grid
      container
      spacing={2}
      justifyContent="space-between"
      alignItems="center"
      columns={12}
      sx={{ flexGrow: 1 }}>
      <Grid xs={12} sm={12} md={6}>
        <Card variant="plain" sx={{ maxWidth: '100%' }}>
          <Typography level="h1" textAlign={'start'}>Get the best deals and offers in your favorite </Typography>
          <Textgradient level="h1" textAlign={'start'}>
            premium stores!
          </Textgradient>
          <Typography textAlign={'justify'}>
            Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality, though, the unity and coherence of ideas among sentences is what constitutes a paragraph.
          </Typography>
        </Card>

        <Stack direction="row" spacing={2}>
          <Button variant="text">
            <img
              src={AppStoreImage}
              srcSet={AppStoreImage}
              alt="App Store"
            />
          </Button>
          <Button variant="text">
            <img
              src={GooglePlayImage}
              srcSet={GooglePlayImage}
              alt="Google Play"
            />
          </Button>
        </Stack>

      </Grid>

      <Grid  xs={12} sm={12} md={6}>
        <AspectRatio
          objectFit="contain"
          variant="plain"
          sx={{
            width: '100%',
          }}>
          <img
            src={LastLP}
            srcSet={LastLP}
            alt="Logo"
          />
        </AspectRatio>
      </Grid>
    </Grid>
  );
}