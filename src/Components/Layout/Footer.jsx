import Link from '@mui/joy/Link';
import Box from '@mui/joy/Box';
import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Typography from '@mui/joy/Typography';



import Card from '@mui/joy/Card';
import AspectRatio from '@mui/joy/AspectRatio';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

// Images
import LogoImage from '../../assets/logo.png';
import AppStoreWhiteImage from '../../assets/Mobileappstorebadge.png';
import GooglePlayWhiteImage from '../../assets/Mobilegoogleplaybadge.png';

// Icons
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';


const EditStyleInstagramIcon = styled(InstagramIcon)(({ theme }) => ({
    color:'#212529'
}));
const EditStyleTwitterIcon = styled(TwitterIcon)(({ theme }) => ({
    color:'#212529'
}));
const EditStyleFacebookIcon = styled(FacebookIcon)(({ theme }) => ({
    color:'#212529'
}));
const EditStyleLinkedInIcon = styled(LinkedInIcon)(({ theme }) => ({
    color:'#212529'
}));

const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));



const ChangeColorLink = styled(Link)(({ theme }) => ({
    // background: '#861889',
    color: '#333',
    ':hover': {
        color: '#861889',
        background: 'none'
    }
}));


export default function Footer() {
    return (

        <Grid container
            spacing={3}
            justifyContent="space-between"
            alignItems="center"
            sx={{ flexGrow: 1, marginTop: '20px' }}>


            <Grid
                container
                spacing={2}
                // justifyContent="space-between"
                alignItems="center"
                justifyContent="space-between"
                columns={12}
                sx={{ flexGrow: 1, marginTop: '100px' }}>
                <Grid xs={12} md={6}>
                    <AspectRatio
                        objectFit="contain"
                        variant="plain"
                        sx={{
                            width: '20%',
                            marginTop: '20px'
                        }}>
                        <img
                            src={LogoImage}
                            srcSet={LogoImage}
                            alt="Logo"
                        />
                    </AspectRatio>
                    <CardStyle variant="plain">
                        <Typography sx={{ textAlign: 'start' }}>
                            Paragraphs are the building blocks of papers. Many students define paragraphs in terms of length: a paragraph is a group of at least five sentences, a paragraph is half a page long, etc. In reality, though, the unity and coherence of ideas among sentences is what constitutes a paragraph.
                        </Typography>
                    </CardStyle>
                </Grid>
                <Grid xs={12} md={4}>
                    <Stack direction="column" spacing={1}>
                        <Button variant="text">
                            <img
                                src={AppStoreWhiteImage}
                                srcSet={AppStoreWhiteImage}
                                alt="App Store"
                            />
                        </Button>
                        <Button variant="text">
                            <img
                                src={GooglePlayWhiteImage}
                                srcSet={GooglePlayWhiteImage}
                                alt="Google Play"
                            />
                        </Button>
                    </Stack>
                </Grid>
            </Grid>


            <Grid xs={12}>
                <Box sx={{ display: 'flex', gap: 3, flexWrap: 'wrap' }}>
                    <ChangeColorLink href="/">Home</ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        About Us
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        How it works
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Faq
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Contact us
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Privacy policy
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Terms & conditions
                    </ChangeColorLink>
                </Box>
                <hr />
            </Grid>

            <Grid xs={12}
                container
                spacing={3}
                direction="row"
                justifyContent="space-between"
                alignItems="center">
                <Grid xs={12} sm={6}>
                    <Typography sx={{ textAlign: 'start' }}>
                        @2022 Premitto LLC. All rights reserved
                    </Typography>
                </Grid>
                


                <Grid xs={12} sm={6}>
                    <Box sx={{ display: 'flex', justifyContent:'flex-end', gap: 3, flexWrap: 'wrap' }}>
                        <Link href="#variants">
                            <EditStyleInstagramIcon />
                        </Link>
                        <Link href="#variants">
                            <EditStyleTwitterIcon/>
                        </Link>
                        <Link href="#variants">
                            <EditStyleFacebookIcon/>
                        </Link>
                        <Link href="#variants">
                            <EditStyleLinkedInIcon/>
                        </Link>
                    </Box>
                </Grid>

            </Grid>

        </Grid>


    );
}