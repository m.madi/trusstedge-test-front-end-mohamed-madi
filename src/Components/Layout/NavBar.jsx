import { useState } from 'react';

import Link from '@mui/joy/Link';
import Box from '@mui/joy/Box';
import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import AspectRatio from '@mui/joy/AspectRatio';
import Button from '@mui/joy/Button';

//////// Munu Mobile
import MenuButton from '@mui/joy/MenuButton';
import Menu from '@mui/joy/Menu';
import MenuItem from '@mui/joy/MenuItem';
import Apps from '@mui/icons-material/Apps';
import Dropdown from '@mui/joy/Dropdown';



import LogoImage from '../../assets/logo.png';

////////// Buttons
const ButtonStyleColor = styled(Button)(({ theme, color = 'primary' }) => ({
    background: '#861889',
    color: '#FFF'
}));

const ChangeColorLink = styled(Link)(({ theme }) => ({
    // background: '#861889',
    color: '#333',
    ':hover': {
        color: '#861889',
        background: 'none'
    }
}));



export default function LinkVariants() {
    const [selectedIndex, setSelectedIndex] = useState(1);

    const createHandleClose = (index) => () => {
        if (typeof index === 'number') {
            setSelectedIndex(index);
        }
    };

    return (

        <>
        <Grid
            container
            spacing={3}
            justifyContent="space-between"
            alignItems="center"
            sx={{ flexGrow: 1, marginTop: '20px' }}>

            <Grid sx={4} className="menuMobile">
                <Dropdown>
                    <MenuButton startDecorator={<Apps />}></MenuButton>
                    <Menu>
                        <ChangeColorLink href="/">
                            <MenuItem
                                {...(selectedIndex === 0 && { selected: true, variant: 'soft' })}
                                onClick={createHandleClose(0)}
                            >
                                Home
                            </MenuItem>
                        </ChangeColorLink>
                        <ChangeColorLink href="/buy">
                            <MenuItem selected={selectedIndex === 2} onClick={createHandleClose(2)}>
                                Buy
                            </MenuItem>
                        </ChangeColorLink>
                        <ChangeColorLink href="#">
                            <MenuItem selected={selectedIndex === 3} onClick={createHandleClose(3)}>
                                Discover Deals
                            </MenuItem>
                        </ChangeColorLink>
                    </Menu>
                </Dropdown>
            </Grid>
            <Grid xs={4} sm={3} md={2}>
                <AspectRatio
                    objectFit="contain"
                    variant="plain"
                    sx={{
                        width: '100%',
                    }}>
                    <img
                        src={LogoImage}
                        srcSet={LogoImage}
                        alt="Logo"
                    />
                </AspectRatio>
            </Grid>
            <Grid xs={6} className="muneDesktop" >
                <Box sx={{ display: 'flex', gap: 3, flexWrap: 'wrap' }}>
                    <ChangeColorLink href="/">Home</ChangeColorLink>
                    <ChangeColorLink href="/buy">Buy</ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Discover Deals
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        How it works
                    </ChangeColorLink>
                    <ChangeColorLink href="#variants" variant="plain">
                        Blog
                    </ChangeColorLink>
                </Box>
            </Grid>

            <Grid xs={4} md={2}>
                <ButtonStyleColor size="md" variant={'solid'} color="primary">
                    Become a partner
                </ButtonStyleColor>
            </Grid>
            
        </Grid>
        <hr/>
        </>


    );
}