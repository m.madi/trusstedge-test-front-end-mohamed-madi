import './App.css';
import { Route, Routes } from 'react-router-dom';
import Grid from '@mui/joy/Grid';


// Components
import NavBar from './Components/Layout/NavBar';
import Footer from './Components/Layout/Footer';
import Container from '@mui/material/Container';


// Pages
import HomePage from './HomePage';
import BuyPage from './BuyPage';

function App() {
  return (
    <div className="App">
      <Container>


        <Grid container spacing={2} sx={{ flexGrow: 1 }}>
          <Grid xs={12} md={12}>
            <NavBar />
          </Grid>

          <Grid xs={12} md={12}>
            <Routes>
            <Route path='/' element={<HomePage />}></Route>
            <Route path='/buy' element={<BuyPage />}></Route>
              
            </Routes>
          </Grid>

          <Grid xs={12} md={12}>
            <Footer/>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default App;
