
import { styled } from '@mui/joy/styles';
import Grid from '@mui/joy/Grid';
import Card from '@mui/joy/Card';
import Typography from '@mui/joy/Typography';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import Button from '@mui/joy/Button';

const Textgradient = styled(Typography)(({ theme }) => ({
    background: '-webkit-linear-gradient(left, #861889, #F8B03A)',
    'border-radius': '20px',
    padding: '7px 5%',
    color: '#FFF'
}));

const TextgradientColor = styled(Typography)(({ theme }) => ({
    background: '-webkit-linear-gradient(right, #861889, #F8B03A)',
    '-webkit-background-clip': 'text',
    '-webkit-text-fill-color': 'transparent'
}));


const CardStyle = styled(Card)(({ theme }) => ({
    background: 'none',
}));


const GridStyle = styled(Grid)(({ theme }) => ({
    // background: '-webkit-linear-gradient(left, #861889, #F8B03A, #F8B03A)',
    borderRadius: '20px',
    border: '1px solid #333'
}));


const ButtonStyleColor = styled(Button)(({ theme, color = 'primary' }) => ({
    background: '#861889',
    color: '#FFF',
    marginTop:'50px'
}));


export default function BuyPage() {
    return (
        <>
            <Grid
                container
                spacing={2}
                // justifyContent="space-between"
                alignItems="center"
                justifyContent="center"
                columns={12}
                sx={{ flexGrow: 1, marginTop: '100px' }}>
                <Grid xs={12} md={5}>
                    <CardStyle variant="plain">
                        <Textgradient level="h5" width={'50%'} margin={'auto'}>Do more and spend less</Textgradient>
                        <Typography level="h1" sx={{ margin: '20px 0' }}>
                            You'll wonder how you ever {' '}
                            <TextgradientColor level="h1" textAlign={'justify'}>
                                lives without it
                            </TextgradientColor>
                        </Typography>
                    </CardStyle>
                </Grid>

                <GridStyle xs={10} sm={6} md={8}
                    container
                    alignItems="center"
                    justifyContent="center"
                >

                    <Grid xs={12} md={12}>
                        <CardStyle variant="plain">
                            <Typography level="h1">
                                79 Sar
                                <Typography level="body-md" textAlign={'center'}>
                                    / month
                                </Typography>
                            </Typography>
                            <Typography level="body-md" textAlign={'center'}>
                                1 Year premium membership
                            </Typography>
                        </CardStyle>
                    </Grid>

                    <Grid xs={7}>
                        <Typography startDecorator={<CheckCircleOutlineIcon />} mb={2}>
                            Includes hundredss of offers across six categories
                        </Typography>
                        <Typography startDecorator={<CheckCircleOutlineIcon />} mb={2}>
                            Includes hundredss of offers across six categories
                        </Typography>
                        <Typography startDecorator={<CheckCircleOutlineIcon />} mb={2}>
                            Includes hundredss of offers across six categories
                        </Typography>
                        <Typography startDecorator={<CheckCircleOutlineIcon />} mb={2}>
                            Includes hundredss of offers across six categories
                        </Typography>
                        <Typography startDecorator={<CheckCircleOutlineIcon />} mb={2}>
                            Includes hundredss of offers across six categories
                        </Typography>

                        <ButtonStyleColor size="lg" fullWidth variant={'solid'} color="primary">
                            Get started
                        </ButtonStyleColor>
                    </Grid>
                </GridStyle>
            </Grid>
        </>
    )
}